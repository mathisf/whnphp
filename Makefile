up:
	docker compose -f compose.yaml -f compose.override.yaml up --pull always --build

down:
	docker compose -f compose.yaml -f compose.override.yaml down -v

up-prod:
	docker compose -f compose.yaml -f compose.prod.yaml build --no-cache
	docker compose -f compose.yaml -f compose.prod.yaml up --force-recreate -d

down-prod:
	docker compose -f compose.yaml -f compose.prod.yaml down

sh: exec-bash
exec-bash:
	docker compose exec php bash

build:
	docker compose build

consume:
	docker compose exec php php bin/console messenger:consume messages -vv

consume-prod:
	docker compose exec php php bin/console messenger:consume messages --time-limit=60

watch-consume-stats:
	watch -n 0.5 docker compose exec php php bin/console messenger:stats

consume-stats:
	docker compose exec php php bin/console messenger:stats
