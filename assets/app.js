/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.css'
import 'iconoir/css/iconoir.css';
import 'inter-ui/inter.css';

export function toggleKids(itemId, directKidIds, allKidIds) {
  const itemElem = document.getElementById(itemId);
  const areKidsDisplayed = itemElem.getAttribute('are-kids-displayed') === 'true';
  const buttonElem = document.getElementById('button-' + itemId);
  buttonElem.innerHTML = buttonElem.innerHTML.trim().replace(/^.{4}/g, !areKidsDisplayed ? 'Hide' : 'Show');
  itemElem.setAttribute('are-kids-displayed', !areKidsDisplayed ? 'true' : 'false');

  const kidIds = areKidsDisplayed ? allKidIds : directKidIds;
  for (const kidId of kidIds) {
      const kidElem = document.getElementById(kidId);
      if (!kidElem) {
          continue;
      }
      kidElem.style.display = areKidsDisplayed ? 'none' : 'flex';
  }
}

window.toggleKids = toggleKids;
