<?php

namespace App\Service;

use App\Dto\Item;
use App\Dto\RawItem;
use App\Dto\RawUser;
use App\Dto\User;
use App\Enum\ItemType;
use App\Enum\ListType;
use Generator;
use App\Message\HnRequestMessage;
use App\MessageHandler\HnRequestMessageHandler;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Component\Messenger\MessageBusInterface;

use function Symfony\Component\String\u;

class HnApiClient
{
    public const ITEM_CACHE_KEY_FORMAT = 'item_%s';
    public const USER_CACHE_KEY_FORMAT = 'user_%s';
    public const ITEM_URI_FORMAT = 'item/%s.json';
    public const USER_URI_FORMAT = 'user/%s.json';
    public const URI_PATH_CLASS_MAP = [
        'user' => RawUser::class,
        'item' => RawItem::class,
    ];

    public function __construct(
        private readonly HttpClientInterface $hnClient,
        private readonly DenormalizerInterface $denormalizer,
        private readonly CacheItemPoolInterface $appCache,
        private readonly MessageBusInterface $bus,

    ) {
    }

    public function getUser(string $username, $loadSubmitted = false): ?User
    {
        $cacheKey = $this->getCacheKeyFromUri($this->toUserUri($username));
        $cacheItem = $this->appCache->getItem($cacheKey);
        $hasDispatchedRequest = false;
        while (!$cacheItem->isHit()) {
            if (!$hasDispatchedRequest) {
                $this->bus->dispatch(new HnRequestMessage(username: $username));
                $hasDispatchedRequest = true;
            }
            usleep(100000); // Sleep for 100ms
            $cacheItem = $this->appCache->getItem($cacheKey);
        }

        $rawUser = $cacheItem->get();
        $user = $rawUser->toUser();
        $user->submitted = $rawUser->submitted;

        return $user;
    }

    public function getList(ListType $listType): array
    {
        return $this->getItems($this->getRawList($listType), false);
    }

    private function getRawList(ListType $listType): array
    {
        $cacheKey = $this->getCacheKeyFromListType($listType->toUrlPath());
        $cacheItem = $this->appCache->getItem($cacheKey);
        $hasDispatchedRequest = false;

        while (!$cacheItem->isHit()) {
            if (!$hasDispatchedRequest) {
                $this->bus->dispatch(new HnRequestMessage(listType: $listType->value, fetchKids: false));

                $hasDispatchedRequest = true;
            }

            usleep(10000); // Sleep for 1000ms
            $cacheItem = $this->appCache->getItem($cacheKey);
        }

        return $cacheItem->get();
    }

    public function getItems(array $itemIds, bool $fetchKids = true, bool $overwriteCache = false): array
    {
        $itemIds = array_unique($itemIds);
        $items = [];
        $queue = new \SplQueue();
        $parentMap = []; // Mapping from child ID to parent ID
        $dispatchedMessageItemIds = [];

        foreach ($itemIds as $itemId) {
            $queue->enqueue($itemId);
        }

        while (!$queue->isEmpty()) {
            $itemId = $queue->dequeue();
            $cacheKey = $this->getCacheKeyFromItemId($itemId);
            $cacheItem = $this->appCache->getItem($cacheKey);

            if (!$cacheItem->isHit() && !in_array($itemId, $dispatchedMessageItemIds)) {
                $dispatchedMessageItemIds[] = $itemId;
                $this->bus->dispatch(new HnRequestMessage(itemId: $itemId, overwriteCache: $overwriteCache, fetchKids: $fetchKids));

                $queue->enqueue($itemId);
                continue;
            } elseif (!$cacheItem->isHit() && in_array($itemId, $dispatchedMessageItemIds)) {
                $queue->enqueue($itemId);
                usleep(10000);
                continue;
            }

            $rawItem = $cacheItem->get();
            $item = $rawItem->toItem();
            $items[$itemId] = $item;

            // Add item to its parent's kids array
            if (isset($parentMap[$itemId])) {
                $parentId = $parentMap[$itemId];
                if (!is_array($items[$parentId]->kids)) {
                    $items[$parentId]->kids = [];
                }
                $items[$parentId]->kids[] = $item;
            }

            // Fetch Kids
            if ($fetchKids && $rawItem->kids && count($rawItem->kids) > 0) {
                foreach ($rawItem->kids as $kidId) {
                    if (!array_key_exists($kidId, $items)) {
                        $queue->enqueue($kidId);
                        $parentMap[$kidId] = $itemId; // Map the child to the parent
                    }
                }
            }
        }

        $rootItems = array_filter($items, function ($item) use ($itemIds) {
            return in_array($item->id, $itemIds);
        });

        return array_values($rootItems);
    }

    public function getItem(
        string|int $itemId,
    ): Item {
        return $this->getItems([$itemId], true)[0];
    }

    public function preloadLists(): void
    {
        foreach (ListType::cases() as $listType) {
            $this->bus->dispatch(new HnRequestMessage(listType: $listType->value, fetchKids: false, overwriteCache: true));
        }
    }

    public function preloadItems(): void
    {
        $itemIds = [];
        foreach (ListType::cases() as $listType) {
            $itemIds = [...$itemIds, ...$this->getRawList($listType)];
        }
        $itemIds = array_unique($itemIds);
        foreach ($itemIds as $itemId) {
            $this->bus->dispatch(new HnRequestMessage(itemId: $itemId, fetchKids: true, overwriteCache: true));
        }
    }

    public function toUserUri(string $username): string
    {
        return sprintf(self::USER_URI_FORMAT, $username);
    }

    public function toListUri(string $listType): string
    {
        return ListType::from($listType)->toUrlPath();
    }

    public function toItemUri(string $itemId): string
    {
        return sprintf(self::ITEM_URI_FORMAT, $itemId);
    }

    public function getCacheKeyFromUri(string $uri): string
    {
        $explodedUri = explode('/', $uri);

        if (count($explodedUri) > 1) {
            $resourceType = str_replace('/', '_', $explodedUri[sizeof($explodedUri) - 2]);
        } else {
            $resourceType = '';
        }

        $resourceId = str_replace('.json', '', $explodedUri[sizeof($explodedUri) - 1]);

        if ($resourceType !== 'item' && $resourceType !== 'user') {
            return $resourceId;
        }

        $cacheKey = str_replace('/', '_', $resourceType) . '_' . $resourceId;

        return $cacheKey;
    }

    public function getCacheKeyFromItemId(string $itemId): string
    {
        return 'item_' . $itemId;
    }

    public function getCacheKeyFromUsername(string $username): string
    {
        return 'user_' . $username;
    }

    public function getCacheKeyFromListType(string $listType): string
    {
        return str_replace('.json', '', $listType);
    }
}
