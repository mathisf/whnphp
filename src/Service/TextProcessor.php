<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class TextProcessor
{
    public function computeTimeAgo(\DateTimeInterface $dateTime): string
    {
        $now = new \DateTime();
        $interval = $now->diff($dateTime);

        if ($interval->y > 0) {
            return $interval->y . ' year' . ($interval->y > 1 ? 's' : '') . ' ago';
        } elseif ($interval->m > 0) {
            return $interval->m . ' month' . ($interval->m > 1 ? 's' : '') . ' ago';
        } elseif ($interval->d > 0) {
            return $interval->d . ' day' . ($interval->d > 1 ? 's' : '') . ' ago';
        } elseif ($interval->h > 0) {
            return $interval->h . ' hour' . ($interval->h > 1 ? 's' : '') . ' ago';
        } elseif ($interval->i > 0) {
            return $interval->i . ' minute' . ($interval->i > 1 ? 's' : '') . ' ago';
        } else {
            return $interval->s . ' second' . ($interval->s > 1 ? 's' : '') . ' ago';
        }
    }

    public function getHostNameFromUrl(string $url): string
    {
        $pattern = '/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/';
        preg_match($pattern, $url, $matches);
        if (empty($matches)) {
            return '';
        } else {
            return str_replace('www.', '', $matches[3]);
        }
    }

    public function processCommentText(string $text, Request $request): string
    {
        $text = $this->replaceOfficialHnUrl($text, $request);
        $text = $this->addGreentextQuoting($text);
        return $text;
    }

    private function replaceOfficialHnUrl(string $text, Request $request): string
    {
        $pattern = '/https?\:(&#x2F;|\/){2}news\.ycombinator\.com(&#x2F;|\/)item\?id\=/mi';
        $replacement = $request->getSchemeAndHttpHost() . '/items/';
        return preg_replace($pattern, $replacement, $text);
    }

    private function addGreentextQuoting(string $text): string
    {
        // Apply "greentext" style to lines starting with '>' or an <i> tag followed by '>'
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);
        // Query for text nodes starting with '>' or contained in an <i> tag directly followed by '>'
        // Also handle cases where '>' is inside the <i> tag or followed by a space
        $nodes = $xpath->query('//text()[starts-with(., ">")] | //i[starts-with(text()[1], ">")]/text() | //i[starts-with(., ">")]/text()');

        foreach ($nodes as $node) {
            $greentextContent = htmlspecialchars($node->nodeValue, ENT_QUOTES, 'UTF-8');
            // Check if the node is a direct child of an <i> tag and starts with '>'
            if (($node->parentNode->nodeName === 'i' && strpos(trim($greentextContent), '>') === 0) || strpos(trim($greentextContent), '> ') === 0) {
                // Remove the leading '>' or '> ' from the content
                $greentextContent = ltrim($greentextContent, '> ');
            }
            $greentext = $dom->createElement('span', $greentextContent);
            $greentext->setAttribute('style', 'color: #789922;');
            $node->parentNode->replaceChild($greentext, $node);
        }

        return $dom->saveHTML();
    }
}
