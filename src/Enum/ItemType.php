<?php

namespace App\Enum;

enum ItemType: string {
    case STORY = 'story';
    case COMMENT = 'comment';
    case JOB = 'job';
    case POLL = 'poll';
    case POLLOPT = 'pollopt';
}
