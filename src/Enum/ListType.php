<?php

namespace App\Enum;

enum ListType: string {
    case TOP = 'top';
    case BEST = 'best';
    case JOB = 'job';
    case ASK = 'ask';
    case SHOW = 'show';
    case NEW = 'new';

    public function toUrlPath(): string {
        return match ($this) {
            $this::TOP => 'topstories.json',
            $this::BEST => 'beststories.json',
            $this::JOB => 'jobstories.json',
            $this::ASK => 'askstories.json',
            $this::SHOW => 'showstories.json',
            $this::NEW => 'newstories.json',
        };
    }
}
