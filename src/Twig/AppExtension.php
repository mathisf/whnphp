<?php

namespace App\Twig;

use App\Service\TextProcessor;
use Symfony\Component\DependencyInjection\Attribute\AsTaggedItem;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function __construct(
        private TextProcessor $textProcessor,
        private RequestStack $requestStack,
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('formatCommentText', [$this, 'formatCommentText']),
            new TwigFilter('formatTimeAgo', [$this, 'formatTimeAgo']),
            new TwigFilter('formatHostName', [$this, 'formatHostName']),
        ];
    }

    public function formatCommentText(string $text): string
    {
        return $this->textProcessor->processCommentText($text, $this->requestStack->getCurrentRequest());
    }

    public function formatTimeAgo(\DateTimeInterface $dateTime): string
    {
        return $this->textProcessor->computeTimeAgo($dateTime);
    }

    public function formatHostName(string $url): string
    {
        return $this->textProcessor->getHostNameFromUrl($url);
    }
}
