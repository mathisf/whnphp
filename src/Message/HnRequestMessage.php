<?php

namespace App\Message;

class HnRequestMessage
{
    public function __construct(
        public string|int|null $itemId = null,
        public string|null $username = null,
        public string|null $listType = null,
        public bool|null $overwriteCache = false,
        public bool $fetchKids = false,
    )
    {

    }
}
