<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\SerializedName;
class RawUser
{
    public function __construct(
        public string $id,
        public ?string $about = null,
        public ?int $created = null,
        public ?int $delay = null,
        public ?int $karma = null,
        public ?array $submitted = null,
    )
    {
    }

    public function toUser(): User {
        return new User(
            $this->id,
            $this->about,
            \DateTimeImmutable::createFromFormat('U', $this->created),
            $this->delay,
            $this->karma,
            null,
        );
    }
}
