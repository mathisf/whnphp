<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\SerializedName;
class User
{
    public function __construct(
        public string $id,
        public ?string $about,
        public ?\DateTimeInterface $created,
        public ?int $delay,
        public ?int $karma,
        /**
         * @var Array<Item> $submitted
         */
        public ?array $submitted,
    )
    {
    }
}
