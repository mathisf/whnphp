<?php

namespace App\Dto;

use App\Enum\ItemType;
class RawItem
{
    public function __construct(
        public int $id,
        public ?bool $deleted = null,
        public ?string $type = null, // We assume type is always one of "job", "story", "comment", "poll", or "pollopt".
        public ?string $by = null,
        public ?int $time = null,
        public ?string $text = null,
        public ?bool $dead = null,
        public ?int $parent = null,
        public ?int $poll = null,
        public ?array $kids = null,
        public ?string $url = null,
        public ?int $score = null,
        public ?string $title = null,
        public ?array $parts = null,
        public ?int $descendants = null,
    )
    {
    }

    public function toItem(): Item {
        return new Item(
            $this->id,
            !!$this->deleted,
            ItemType::tryFrom($this->type),
            $this->by,
            \DateTimeImmutable::createFromFormat('U', $this->time),
            !!$this->dead,
            $this->text,
            $this->parent,
            $this->poll,
            null,
            $this->url,
            $this->score,
            $this->title,
            $this->parts,
            $this->descendants,
        );
    }
}
