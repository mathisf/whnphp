<?php

namespace App\Dto;

use App\Enum\ItemType;
use Symfony\Component\Serializer\Annotation\SerializedName;
class Item
{
    public function __construct(
        public int $id,
        public bool $deleted,
        public ItemType $type, // We assume type is always one of "job", "story", "comment", "poll", or "pollopt".
        public ?string $by,
        public \DateTimeInterface $time,
        public bool $dead,
        public ?string $text,
        public ?int $parent,
        public ?int $poll,
        /**
         * @var Array<Item> $submitted
         */
        public ?array $kids,
        public ?string $url,
        public ?int $score,
        public ?string $title,
        public ?array $parts,
        public ?int $descendants,
    )
    {
    }

    public function getKidsIds(): array
    {
        return array_map(fn($kid) => $kid->id, $this->kids ?? []);
    }

    public function getKidsIdsRecursive(): array
    {
        $ids = [];
        $stack = $this->kids ?? [];
        while ($stack) {
            $current = array_pop($stack);
            $ids[] = $current->id;
            if ($current->kids) {
                $stack = array_merge($stack, $current->kids);
            }
        }
        return $ids;
    }
}
