<?php

namespace App\Controller;

use App\Enum\ListType;
use App\Service\HnApiClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\ResponseInterface;

#[AsController]
class HnController extends AbstractController
{
    public function __construct(
        private HnApiClient $hnApiClient,
    ) {}

    #[Route(path: '/')]
    public function index(): Response
    {
        return $this->redirect('/top');
    }

    #[Route(name: 'list', path: '/{listType}', requirements: ['listType' => 'best|top|job|ask|show|new'])]
    public function list(?ListType $listType = ListType::TOP): Response
    {
        return $this->render(
            'pages/list.html.twig',
            [
                'items' => $this->hnApiClient->getList($listType),
                'list_type' => $listType,
                'sorted_by_time' => false,
                'show_time_menu_item' => true,
            ]
        );
    }

    #[Route(name: 'list_sorted_by_time', path: '/time/{listType}', requirements: ['listType' => 'best|top|job|ask|show|new'])]
    public function listSortedByTime(?ListType $listType = ListType::TOP): Response
    {
        $timeGroupedAndSortedItems = array_reduce($this->hnApiClient->getList($listType), function ($carry, $item) {
            $dateKey = $item->time->format('Y-m-d');
            if (!isset($carry[$dateKey])) {
                $carry[$dateKey] = [];
            }
            $carry[$dateKey][] = $item;
            return $carry;
        }, []);

        krsort($timeGroupedAndSortedItems);

        return $this->render(
            'pages/list_sorted_by_time.html.twig',
            [
                'time_grouped_and_sorted_items' => $timeGroupedAndSortedItems,
                'list_type' => $listType,
                'sorted_by_time' => true,
                'show_time_menu_item' => true,
            ]
        );
    }

    #[Route(name: 'item', path: '/items/{id}')]
    public function item(string $id): Response
    {
        return $this->render(
            'pages/item.html.twig',
            [
                'item' => $this->hnApiClient->getItem($id),
                'sorted_by_time' => false,
                'show_time_menu_item' => false,
            ]
        );
    }

    #[Route(name: 'user', path: '/users/{id}')]
    public function user(string $id): Response
    {
        return $this->render(
            'pages/user.html.twig',
            [
                'user' => $this->hnApiClient->getUser($id),
                'sorted_by_time' => false,
                'show_time_menu_item' => false,
            ]
        );
    }

    #[Route(name: 'about', path: '/about')]
    public function about(): Response
    {
        return $this->render(
            'pages/about.html.twig',
            [
                'sorted_by_time' => false,
                'show_time_menu_item' => false,
            ]
        );
    }
}
