<?php

namespace App\MessageHandler;

use App\Message\HnRequestMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\Messenger\Handler\Acknowledger;
use Symfony\Component\Messenger\Handler\BatchHandlerInterface;
use Symfony\Component\Messenger\Handler\BatchHandlerTrait;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Dto\Item;
use App\Dto\RawItem;
use App\Dto\RawUser;
use App\Dto\User;
use App\Enum\ItemType;
use App\Enum\ListType;
use App\Service\HnApiClient;
use Generator;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class HnRequestMessageHandler implements BatchHandlerInterface
{
    use BatchHandlerTrait;

    public function __construct(
        protected CacheItemPoolInterface $appCache,
        protected HttpClientInterface $hnClient,
        protected HnApiClient $hnApiClient,
        protected DenormalizerInterface $denormalizer,
        protected MessageBusInterface $bus,
        #[Autowire(env: 'int:HN_API_REQUESTS_CHUNK_SIZE')]
        protected readonly int $hnApiRequestsChunkSize = 200,
    ) {
    }

    public function __invoke(HnRequestMessage $message, Acknowledger $ack = null): mixed
    {
        return $this->handle($message, $ack);
    }

    protected function process(array $jobs): void
    {
        $responses = [];
        $acks = [];
        $handledUriPaths = [];
        $overwriteCacheMap = [];
        $fetchKidsMap = [];

        /** @var HnRequestMessage $message */
        foreach ($jobs as [$message, $ack]) {
            if ($message->itemId) {
                $uri = $this->hnApiClient->toItemUri($message->itemId);
            } elseif ($message->username) {
                $uri = $this->hnApiClient->toUserUri($message->username);
            } elseif ($message->listType) {
                $uri = $this->hnApiClient->toListUri($message->listType);
            }
            $cacheKey = $this->hnApiClient->getCacheKeyFromUri($uri);
            if (in_array($uri, $handledUriPaths)) {
                $ack->ack($message);
                continue;
            }
            $handledUriPaths[] = $uri;
            $overwriteCacheMap[$cacheKey] = $message->overwriteCache;
            $fetchKidsMap[$cacheKey] = $message->fetchKids;
            $responses[] = $this->hnClient->request('GET', $uri);
            $acks[$cacheKey] = $ack;
        }

        foreach ($this->hnClient->stream($responses) as $response => $chunk) {
            if ($chunk->isLast()) {
                try {
                    $cacheKey = $this->hnApiClient->getCacheKeyFromUri($response->getInfo('url'));
                    $cacheItem = $this->appCache->getItem($cacheKey);
                    if (str_starts_with($cacheKey, 'item')) {
                        $result = $this->denormalizer->denormalize(
                            json_decode($response->getContent(), true),
                            RawItem::class,
                        );
                    } elseif (str_starts_with($cacheKey, 'user')) {
                        $result = $this->denormalizer->denormalize(
                            json_decode($response->getContent(), true),
                            RawUser::class,
                        );
                    // else its a list
                    } else {
                        $result = json_decode($response->getContent(), true);
                        $this->loadWholeList($result);
                    }

                    $cacheItem->set($result);
                    $this->appCache->save($cacheItem);

                    if ($result instanceof RawItem) {
                        if ($fetchKidsMap[$cacheKey] && $result->kids && count($result->kids ?? []) > 0) {
                            foreach ($result->kids as $itemId) {
                                if (
                                    $overwriteCacheMap[$cacheKey]
                                    || !$this->appCache->hasItem($this->hnApiClient->getCacheKeyFromItemId($itemId))
                                ) {
                                    $this->bus->dispatch(new HnRequestMessage(
                                        itemId: $itemId,
                                        overwriteCache: $overwriteCacheMap[$cacheKey],
                                    ));
                                }
                            }
                        }

                        if ($result->by) {
                            if (
                                !$this->appCache->hasItem($this->hnApiClient->getCacheKeyFromUsername($result->by))
                            ) {
                                $this->bus->dispatch(new HnRequestMessage(
                                    username: $result->by,
                                ));
                            }
                        }

                        if ($result->parent) {
                            // if (
                            //     !$this->appCache->hasItem($this->hnApiClient->getCacheKeyFromItemId($result->parent))
                            // ) {
                            //     $this->bus->dispatch(new HnRequestMessage(
                            //         itemId: $result->parent,
                            //     ));
                            // }
                        }
                    }

                    if ($result instanceof RawUser && count($result->submitted ?? []) > 0) {
                        // foreach ($result->submitted as $itemId) {
                        //     if (
                        //         !$this->appCache->hasItem($this->hnApiClient->getCacheKeyFromItemId($itemId))
                        //     ) {
                        //         $this->bus->dispatch(new HnRequestMessage(
                        //             itemId: $itemId,
                        //         ));
                        //     }
                        // }
                    }

                    $acks[$cacheKey]?->ack($result);
                } catch (\Throwable $e) {
                    $acks[$cacheKey]?->nack($e);
                }
            }
        }
    }

    private function getBatchSize(): int
    {
        return $this->hnApiRequestsChunkSize;
    }

    // Prevents timeouts
    private function loadWholeList(array $itemIds): void {
        foreach ($itemIds as $itemId) {
            $uri = $this->hnApiClient->toItemUri($itemId);
            $responses[] = $this->hnClient->request('GET', $uri);
        }

        foreach ($this->hnClient->stream($responses) as $response => $chunk) {
            if ($chunk->isLast()) {
                try {
                    $cacheKey = $this->hnApiClient->getCacheKeyFromUri($response->getInfo('url'));
                    $cacheItem = $this->appCache->getItem($cacheKey);

                    $rawItem = $this->denormalizer->denormalize(
                        json_decode($response->getContent(), true),
                        RawItem::class,
                    );

                    $cacheItem->set($rawItem);
                    $this->appCache->save($cacheItem);
                } catch (\Throwable $e) {
                }
            }
        }
    }
}
