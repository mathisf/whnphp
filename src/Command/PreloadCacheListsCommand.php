<?php

namespace App\Command;

use App\Enum\ListType;
use App\Service\HnApiClient;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Scheduler\Attribute\AsCronTask;

#[AsCommand(
    name: 'app:preload-cache:lists',
    description: 'Preload HN API cache',
)]
#[AsCronTask(expression: '*/15 * * * *', method: 'preload')]
class PreloadCacheListsCommand extends Command
{
    public function __construct(
        private HnApiClient $hnApiClient,
    )
    {
        parent::__construct();
    }
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->hnApiClient->preloadLists();

        return Command::SUCCESS;
    }

    public function preload() {
        $this->hnApiClient->preloadLists();
    }
}
